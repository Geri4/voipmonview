<?php
/**
 * Created by PhpStorm.
 * User: kilex
 * Date: 07.05.15
 * Time: 12:31
 */

include_once('mysqlc.php');

include_once('config.php');

function tomin($time) {
    $sec = $time % 60;
    $time = floor($time / 60);
    $min = $time % 60;
    $time = floor($time / 60);
    return "$min:$sec";
}

$mysql_connection=new SafeMySQL($sqlopt);

$memd = new Memcache;
$memd->connect('localhost', 11211) or die ("Не могу подключиться к кешу");


$respidscache=$memd->get("respids");
if ($respidscache) {
    $responseids=$respidscache;
//    print "respids cache<br>";

}
else {
    $query="SELECT * FROM voipmonitor.cdr_sip_response;";
    $responseids=$mysql_connection->getInd("id",$query);
    $memd->set("respids",$responseids,false,600);
}

$ipgroupscache=$memd->get("ipgroups");
if ($ipgroupscache) {
    $ipgroups=$ipgroupscache;
//    print "ipscache<br>";
}
else {
    $query="SELECT * FROM voipmonitor.cb_ip_groups;";
    $ipgroups=$mysql_connection->getInd("ip",$query);
    $memd->set("ipgroups",$ipgroups,false,600);
}

$uaarray=$memd->get("ua");
if (!$uaarray) {
    $query="SELECT * FROM voipmonitor.cdr_ua;";
    $uaarray=$mysql_connection->getInd("id",$query);
    $memd->set("ua",$uaarray,false,600);
}

//print_r($responseids);

$debugid=$_POST['id'];

if (!$debugid) {
    die ('Без ID не работаем!');
}


$query="select
  cdr.ID,
  cdr.calldate,
  cdr.duration,
  cdr.connect_duration,
  cdr.caller,
  cdr.called,
  lastSIPresponse_id,
  lastSIPresponseNum,
  inet_ntoa(cdr.sipcallerip) as callerip,
  inet_ntoa(cdr.sipcalledip) as calledip,
  mos_min_mult10,
  payload,
  a_payload,
  b_payload,
  a_ua_id,
  b_ua_id,
  cdr.delay_sum,
  cdr.whohanged,
  CONCAT(DATE_FORMAT(cdr.calldate,'%Y-%m-%d/%H/%i/SIP/'), cdr_next.fbasename,'.pcap') as filepath,
  CONCAT(DATE_FORMAT(cdr.calldate,'%Y-%m-%d/%H/%i/RTP/'), cdr_next.fbasename,'.pcap') as filepathrtp
  from cdr
  left join cdr_next on cdr.ID = cdr_next.cdr_ID
  where cdr.ID=$debugid
  order by cdr.calldate desc
  limit 10;";

//print $query."<br>";

$querycache=$memd->get("SQL$debugid");
if ($querycache) {
    $result=$querycache;
//    print "<span class='label label-info'>from cache!</span>";
}
else {
    $result=$mysql_connection->getRow($query);
    $memd->set("SQL$debugid",$result,false,600);
//    print "not from cache (";
}

//$report=print_r($result,true);
$report .="<b>Calldate:</b> $result[calldate]<br>";
$duration=tomin($result[duration]);
$durationc=tomin($result[connect_duration]);
$report .="<b>Duration:</b> $duration / $durationc<br>";
$calleripname=$ipgroups[$result[callerip]]['descr'];
$calledipname=$ipgroups[$result[calledip]]['descr'];
$report .="<b>Caller:</b> <span class='label label-warning'>$result[caller]</span> / <span class='label label-info'>$result[callerip]</span> <span class='label label-info'>$calleripname</span> ".$uaarray[$result[a_ua_id]]['ua']."<br>";
$report .="<b>Called:</b> <span class='label label-default'>$result[called]</span> / <span class='label label-primary'>$result[calledip]</span> <span class='label label-primary'>$calledipname</span> ".$uaarray[$result[b_ua_id]]['ua']."<br>";



if ($payloadtypes[$result['payload']]) $payload=$payloadtypes[$result['payload']];
else $payload=$result['payload'];
if ($payloadtypes[$result['a_payload']]) $apayload=$payloadtypes[$result['a_payload']];
else $apayload=$result['a_payload'];
if ($payloadtypes[$result['b_payload']]) $bpayload=$payloadtypes[$result['b_payload']];
else $bpayload=$result['b_payload'];


$report .="<b>Payload:</b> <span class='label label-success'>$payload</span> <span class='label label-warning'>$apayload</span> <span class='label label-default'>$bpayload</span><br>"; // 8 G.711a 18 G.729
$report .="<b>Who hanged:</b> $result[whohanged]<br>";

if ($result['delay_sum']<1000) $delaystyle='success';
elseif ($result['delay_sum']<10000) $delaystyle='warning';
else $delaystyle='danger';

$report .="<b>Delay:</b> <span class='label label-$delaystyle'>$result[delay_sum]</span><br>";

$loss=$result['mos_min_mult10'];
if ($loss>39) $rtcpstyle='success';
elseif ($loss>29) $rtcpstyle='warning';
else $rtcpstyle='danger';
if ($loss) $report.="<b>MOS:</b> <span class='label label-$rtcpstyle'>$loss</span><br>";



print "$report<hr>";



$filename=$pcapfolder.$result['filepath'];
//print $filename;
print "<span class='tshark' data-filename='$filename' id='tshark'>Тишаркинг...</span>";

print "<hr><a href=./pcap/$result[filepath]>Скачать pcap sip трафика</a><br>";
print "<a href=./pcap/$result[filepathrtp]>Скачать pcap rtp трафика</a><br>";

print "<hr>";
print "<h4>Вся запись в voipmonitor:</h4>";
$query="select
  *
  from cdr
  where cdr.ID=$debugid
  order by cdr.calldate desc
  limit 1;";

$fullquerycache=$memd->get("fullSQL$debugid");
if ($fullquerycache) {
    $fullresult=$fullquerycache;
    print "<span class='label label-info'>from cache!</span>";
}
else {
    $fullresult=$mysql_connection->getRow($query);
    $memd->set("fullSQL$debugid",$fullresult,false,600);
//    print "not from cache (";
}

$report=print_r($fullresult,true);
print "<pre>$report</pre>";

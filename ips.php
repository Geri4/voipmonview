<?php
/**
 * Created by PhpStorm.
 * User: kilex
 * Date: 17.01.14
 * Time: 12:31
 */

include_once('mysqlc.php');

include_once('config.php');


$mysql_connection=new SafeMySQL($sqlopt);

$memd = new Memcache;
$memd->connect('localhost', 11211) or die ("Не могу подключиться к кешу");


$respidscache=$memd->get("respids");
if ($respidscache) {
    $responseids=$respidscache;
//    print "respids cache<br>";

}
else {
    $query="SELECT * FROM voipmonitor.cdr_sip_response;";
    $responseids=$mysql_connection->getInd("id",$query);
    $memd->set("respids",$responseids,false,600);
}

$ipgroupscache=$memd->get("ipgroups");
if ($ipgroupscache) {
    $ipgroups=$ipgroupscache;
//    print "ipscache<br>";
}
else {
    $query="SELECT * FROM voipmonitor.cb_ip_groups;";
    $ipgroups=$mysql_connection->getInd("ip",$query);
    $memd->set("ipgroups",$ipgroups,false,600);
}

$uaarray=$memd->get("ua");
if (!$uaarray) {
    $query="SELECT * FROM voipmonitor.cdr_ua;";
    $uaarray=$mysql_connection->getInd("id",$query);
    $memd->set("ua",$uaarray,false,600);
}

//print_r($responseids);
print "<div class='container'><table class='table'><tr><th>Server</th><th>IP</th><th>Filter as</th></tr>";
foreach ($ipgroups as $val) {
    print "<tr><td>$val[descr]</td><td>$val[ip]</td><td>
        <a href='#' class='callerip btn btn-xs btn-info' data-num='$val[ip]'>Caller IP</a>
        <a href='#' class='calledip btn btn-xs btn-primary' data-num='$val[ip]'>Called IP</a>
        </td></tr>";
}
print "</table></div>";

<!DOCTYPE html>
<head>
    <link href="dist/css/bootstrap.css" rel="stylesheet">
    <link href="dist/css/bootstrap-theme.css" rel="stylesheet">
    <link href="dist/css/datepicker.css" rel="stylesheet">
    <?php include_once('config.php'); ?>
    <title>VMV</title>

    <meta charset="utf-8">
<style>

    .container-full {
        margin: 0 auto;
        width: 100%;
        padding-bottom: 50px;
        padding-left: 10px;
        padding-right: 10px;
    }
</style>

    <style type="text/css">

        html,
        body {
            height: 100%;
            /* The html and body elements cannot have any padding or margin. */
        }

        /* Wrapper for page content to push down footer */
        #wrap {
            min-height: 100%;
            height: auto !important;
            height: 100%;
            /* Negative indent footer by it's height */
            margin: 0 auto -60px;
        }


        #footer {
            height: 60px;
        }
        #footer {
            background-color: #f5f5f5;
        }
    </style>
</head>
<body>


        <div class="navbar navbar-default navbar-fixed-top">
            <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="./" style="<?php echo $brandstyle;?>"><?php echo $brand;?></a>
            </div>


            </div>
        </div>
    <div class="container-full" id="wrap">
        <div class="jumbotron" style="padding-bottom: 15px; font-size: inherit; font-weight: inherit;" id="header">
            <div class="container">
                <div class="row" style="margin-top: 10px;">
                    <div class="col-md-12">
                        <center>
                            <h2 style="margin-top: 5px; margin-bottom: 0px;">
                                VoipMonitor Viewer
                            </h2>

                        </center>
                    </div>
                </div>
            </div>
        </div>

        <?php

        if (!$_GET['debug']){
            print '<div class="container"><form class="form-inline" role="form">';
            print '  <div class="form-group">
                <input type="text" class="form-control datepicker" data-date-format="yyyy-mm-dd" data-date-weekStart="1" data-date-startDate="2014-06-10" data-date-endDate="+1d" data-date-language="ru" data-date-autoclose="true" id="datesr" placeholder="Date">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" id="timefrom" placeholder="00:00:00">
            </div>
            - <div class="form-group">
                <input type="text" class="form-control" id="timeto" placeholder="23:59:59">
            </div>
            <br>';
            print '
            <div class="form-group">
                <input type="text" class="form-control" id="caller" placeholder="caller">
            </div>';
            print '
            -> <div class="form-group">
                <input type="text" class="form-control" id="called" placeholder="called">
            </div>';
            print  ' <br>
            <div class="form-group">
                <input type="text" class="form-control" id="callerip" placeholder="callerip">
            </div>';
            print '
            -> <div class="form-group">
                <input type="text" class="form-control" id="calledip" placeholder="calledip">
            </div>';
            //TODO Сделать поиск по нажатию ентера
            print '<br><br><button type="button" class="btn btn-success history-search">Search</button>
            <!-- <input type="reset" class="btn btn-warning history-search"> -->
            <a href="./" class="btn btn-primary">Home</a>
        </form></div><br>';
            print '<div id="fulllist"></div>';
        }
        elseif ($_GET['debug']) {
            print "<h1>Detail</h1>";
            print "<span class='debug' data-id='$_GET[debug]' id='$_GET[debug]'>Getting from DB $_GET[debug]...</span>";
        }


        ?>


    </div>
        <div id="footer">
            <div class="navbar navbar-default container">
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li>
                            <h3><small><?php echo $footer; ?></small></h3>
                        </li>
                    </ul>
                </div>

            </div>

        <script src="dist/js/jquery.js"></script>
        <script src="dist/js/bootstrap.js"></script>
        <script src="dist/js/bootstrap.min.js"></script>
        <script src="dist/js/modal.js"></script>
        <script src="dist/js/popover.js"></script>
        <script src="dist/js/tooltip.js"></script>


        <script language="JavaScript" src="main.js">
        </script>

<!--            <script src="/audiojs/audio.min.js"></script>-->
            <script src="dist/js/bootstrap-datepicker.js"></script>
            <script src="dist/js/locales/bootstrap-datepicker.ru.js"></script>

</body>
<?php
/**
 * Created by PhpStorm.
 * User: kilex
 * Date: 7.05.15
 * Time: 12:31
 */


$memd = new Memcache;
$memd->connect('localhost', 11211) or die ("Не могу подключиться к кешу");


$filename=$_POST['filename'];

if (!$filename) {
    die ('Без filename не работаем!');
}


//$tsharkcache=$memd->get("tshark$filename");
$tsharkxml=$memd->get("tsharkxml$filename");

//if ($tsharkcache) {
//    $tshark=$tsharkcache;
//    print "<span class='label label-info'>from cache!</span>";
//}
//else {
//    $tshark=`tshark -t ad -nr $filename`;
//    $memd->set("tshark$filename",$tshark,false,16000);
//}

if (!$tsharkxml) {
    $tsharkxml=`tshark -T pdml -t ad -nr $filename`;
    $memd->set("tsharkxml$filename",$tsharkxml,false,16000);
}


//print "<pre>$tshark</pre>";


$xslDoc = new DOMDocument();
$xslDoc->load("dist/pdml2html.xsl");

$xmlDoc = new DOMDocument();
$xmlDoc->loadXML($tsharkxml);

$proc = new XSLTProcessor();
$proc->importStylesheet($xslDoc);
echo $proc->transformToXML($xmlDoc);

//echo "Index array\n";
//print_r($index);
//echo "\nМассив Vals\n";
//$xml = new SimpleXMLElement($tsharkxml);
//print "<pre>";

//foreach ($xml->packet as $element) {
//    print "!!!!";
////    print_r($element->proto);
//    foreach ($element as $type) {
//        print "+++++";
//        print_r($type);
//    }
//}

//print_r($index);
//print_r($vals);
//print "</pre>";
